package com.se.excel.builder;

import java.awt.Color;
import java.awt.Font;

import org.apache.poi.ss.usermodel.CellStyle;

import com.se.excel.builder.template.CellStyleTemplate;


public class DefaultCellStyle extends CellStyleTemplate {

	private Color fillBackgroundColor = Color.WHITE;
	private Color fillFontColor = Color.BLACK;
	private short alignment = CellStyle.ALIGN_CENTER;
	private short verticalAlignment = CellStyle.VERTICAL_CENTER;
	private short borderBottom = CellStyle.BORDER_THIN;
	private short borderLeft = CellStyle.BORDER_THIN;
	private short borderRight = CellStyle.BORDER_THIN;
	private short borderTop = CellStyle.BORDER_THIN;
	private short border = CellStyle.BORDER_THIN;
	private Font font;
	private boolean isWrapped;
	private boolean isUnderLined;

	DefaultCellStyle() {
		this(Color.WHITE, Color.BLACK, CellStyle.ALIGN_CENTER,
				CellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN,
				CellStyle.BORDER_THIN, CellStyle.BORDER_THIN,
				CellStyle.BORDER_THIN, new Font("Calibri", java.awt.Font.PLAIN,
						10), false);
	}

	DefaultCellStyle(Color fillBackgroundColor, Color fillFontColor,
			short alignment, short verticalAlignment, short borderBottom,
			short borderLeft, short borderRight, short borderTop, Font font,
			boolean isWrapped) {
		this.fillBackgroundColor = fillBackgroundColor;
		this.fillFontColor = fillFontColor;
		this.alignment = alignment;
		this.verticalAlignment = verticalAlignment;
		this.borderBottom = borderBottom;
		this.borderLeft = borderLeft;
		this.borderRight = borderRight;
		this.borderTop = borderTop;
		this.font = font;
		this.isWrapped = isWrapped;
	}

	public Color getFillBackgroundColor() {
		return fillBackgroundColor;
	}

	public void setFillBackgroundColor(Color fillBackgroundColor) {
		this.fillBackgroundColor = fillBackgroundColor;
	}

	public Color getFillFontColor() {
		return fillFontColor;
	}

	public void setFillFontColor(Color fillFontColor) {
		this.fillFontColor = fillFontColor;
	}

	public short getAlignment() {
		return alignment;
	}

	public void setAlignment(short alignment) {
		this.alignment = alignment;
	}

	public short getVerticalAlignment() {
		return verticalAlignment;
	}

	public void setVerticalAlignment(short verticalAlignment) {
		this.verticalAlignment = verticalAlignment;
	}

	public short getBorderBottom() {
		return borderBottom;
	}

	public void setBorderBottom(short borderBottom) {
		this.borderBottom = borderBottom;
	}

	public short getBorderLeft() {
		return borderLeft;
	}

	public void setBorderLeft(short borderLeft) {
		this.borderLeft = borderLeft;
	}

	public short getBorderRight() {
		return borderRight;
	}

	public void setBorderRight(short borderRight) {
		this.borderRight = borderRight;
	}

	public short getBorderTop() {
		return borderTop;
	}

	public void setBorderTop(short borderTop) {
		this.borderTop = borderTop;
	}

	public void setBorder(short border) {
		if (this.borderBottom == CellStyle.BORDER_THIN) {
			this.borderBottom = border;
		}
		if (this.borderLeft == CellStyle.BORDER_THIN) {
			this.borderLeft = border;
		}
		if (this.borderRight == CellStyle.BORDER_THIN) {
			this.borderRight = border;
		}
		if (this.borderTop == CellStyle.BORDER_THIN) {
			this.borderTop = border;
		}
		this.border = border;
	}

	public short getBorder() {
		return border;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public void setWrapped(boolean isWrapped) {
		this.isWrapped = isWrapped;
	}

	public boolean isWrapped() {
		return isWrapped;
	}

	@Override
	public void setUnderLined(boolean isUnderLined) {
		this.isUnderLined = isUnderLined;
		
	}

	@Override
	public boolean isUnderLined() {
		return this.isUnderLined;
	}

}
