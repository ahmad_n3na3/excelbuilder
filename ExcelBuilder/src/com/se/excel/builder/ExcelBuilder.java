package com.se.excel.builder;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;

import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.CellTemplate;
import com.se.excel.builder.template.ImageTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.builder.template.RowTemplate;
import com.se.excel.builder.template.SheetAttribute;
import com.se.excel.builder.template.SheetTemplate;
import com.se.excel.builder.utilities.ExportUtils;
import com.se.excel.exceptions.StyleNumberExceedException;

/**
 * @author ahmad_neanaa
 * 
 */
public class ExcelBuilder {

	private int styleCount;
	private List<SheetTemplate> sheetTemplates = new ArrayList<SheetTemplate>();
	private CellStyleTemplate defultHeaderStyleTemplate = null;
	private CellStyleTemplate defultDataStyleTemplate = null;
	private CellStyleTemplate linkStyleTemplate = null;
	private CellStyleTemplate centeredStyle = null;
	public int lastColumnWriten = 6;
	
	/**
	 * Creates sheet template with report name, sheet name,header ,data and some
	 * attributes
	 * 
	 * @param sheetName
	 * @param reportName
	 * @param header
	 * @param datatList
	 * @param reportInformation
	 * @return {@link SheetTemplate} object
	 */
	public SheetTemplate createSheet(String sheetName, String reportName,
			List<String> header, List<List<String>> datatList,
			SheetAttribute... reportInformation) {
		SheetTemplate sheetTemplate = new DefaultSheet();
		sheetTemplate.setReportName(reportName);
		sheetTemplate.setSheetName(sheetName);
		sheetTemplate.setReportInformation(Arrays.asList(reportInformation));
		sheetTemplates.add(sheetTemplate);
		if ((header != null && !header.isEmpty())
				|| (datatList != null && !datatList.isEmpty())) {

			sheetTemplate.setDataList(new ArrayList<RowTemplate>());
			RowTemplate rowTemplate = createRow();
			rowTemplate.setCells(createCells(header));
			rowTemplate.setHeader(true);
			sheetTemplate.getDataList().add(rowTemplate);

			if (datatList != null) {
				for (int i = 0; i < datatList.size(); i++) {
					rowTemplate = createRow();
					rowTemplate.setCells(createCells(datatList.get(i)));
					sheetTemplate.getDataList().add(rowTemplate);
				}
			}

		}

		return sheetTemplate;
	}

	/**
	 * Creates sheet template with report name, sheet name and some attributes
	 * 
	 * @param sheetName
	 * @param reportName
	 * @param reportInformation
	 *            like user name,date or/and etc...
	 * @return {@link SheetTemplate} object
	 */

	public SheetTemplate createSheet(String sheetName, String reportName,
			SheetAttribute... reportInformation) {
		return createSheet(sheetName, reportName, null, null, reportInformation);
	}

	/**
	 * Creates sheet template with only report name and sheet name
	 * 
	 * @param sheetName
	 * @param reportName
	 * @return {@link SheetTemplate} object
	 */
	public SheetTemplate createSheet(String sheetName, String reportName) {

		return createSheet(sheetName, reportName, new SheetAttribute());
	}

	/**
	 * create a row template object with start cell number
	 * 
	 * @return {@link RowTemplate} object
	 */
	public RowTemplate createRow() {
		return createRow(0);
	}

	/**
	 * @param startColumn
	 * @return
	 */
	public RowTemplate createRow(int startColumn) {
		RowTemplate rowTemplate = new DefaultRow();
		rowTemplate.setStartColumn(startColumn);
		return rowTemplate;
	}

	/**
	 * @return
	 */
	public RowTemplate createBlankRow() {
		RowTemplate rowTemplate = new DefaultRow();
		rowTemplate.setBlank(true);
		return rowTemplate;
	}

	/**
	 * @return
	 */
	public CellTemplate createCell() {
		return createCell(null, null, false, null);
	}

	public CellTemplate createCell(String value) {
		return createCell(value, null, false, null);
	}

	public CellTemplate createCell(String value, boolean isLinkable,
			String linkLabel) {
		return createCell(value, null, isLinkable, linkLabel);
	}

	public CellTemplate createCell(String value,
			CellStyleTemplate cellStyleTemplate, boolean isLinkable,
			String linkLabel) {
		CellTemplate cellTemplate = new DefaultCell();
		cellTemplate.setCellStyleTemplate(cellStyleTemplate);
		cellTemplate.setCellValue(value);
		cellTemplate.setLinkable(isLinkable);
		cellTemplate.setLinkLabel(linkLabel);
		return cellTemplate;
	}

	/**
	 * @param cellValues
	 * @return
	 */
	public List<CellTemplate> createCells(String... cellValues) {
		List<CellTemplate> cellTemplates = new ArrayList<CellTemplate>();

		if (cellValues != null && cellValues.length > 0) {
			for (String cellValue : cellValues) {
				CellTemplate cellTemplate = createCell();
				cellTemplate.setCellValue(cellValue);
				cellTemplates.add(cellTemplate);
			}
		}

		return cellTemplates;
	}

	/**
	 * @param cellValues
	 * @return
	 */
	public List<CellTemplate> createCells(List<String> cellValues) {
		List<CellTemplate> cellTemplates = new ArrayList<CellTemplate>();

		if (cellValues != null && cellValues.size() > 0) {
			for (String cellValue : cellValues) {
				CellTemplate cellTemplate = createCell();
				cellTemplate.setCellValue(cellValue);
				cellTemplates.add(cellTemplate);
			}
		}

		return cellTemplates;
	}

	/**
	 * @return
	 * @throws StyleNumberExceedException
	 */
	public CellStyleTemplate createStyle() throws StyleNumberExceedException {
		if (styleCount == 39) {
			throw new StyleNumberExceedException(
					"Styles cannot exceed 40 style per workbook");
		} else {
			styleCount++;
		}

		return new DefaultCellStyle();
	}

	/**
	 * @param workbook
	 * @param usedIndexsList
	 * @return
	 * @throws StyleNumberExceedException
	 */
	public CellStyleTemplate getDefultHeaderStyleTemplate()
			throws StyleNumberExceedException {
		if (defultHeaderStyleTemplate == null) {

			short border = CellStyle.BORDER_MEDIUM;
			defultHeaderStyleTemplate = this.createStyle();
			defultHeaderStyleTemplate.setFillBackgroundColor(new Color(127,
					127, 127));
			defultHeaderStyleTemplate.setFillFontColor(Color.WHITE);
			defultHeaderStyleTemplate.setBorderBottom(border);
			defultHeaderStyleTemplate.setBorderLeft(border);
			defultHeaderStyleTemplate.setBorderRight(border);
			defultHeaderStyleTemplate.setBorderTop(border);
			defultHeaderStyleTemplate.setFont(new java.awt.Font("Calibri",
					java.awt.Font.BOLD, 10));
			defultHeaderStyleTemplate.setAlignment(CellStyle.ALIGN_CENTER);
		}
		return defultHeaderStyleTemplate;
	}

	/**
	 * @param workbook
	 * @param usedIndexsList
	 * @return
	 * @throws StyleNumberExceedException
	 */
	public CellStyleTemplate getDefultDatasStyleTemplate()
			throws StyleNumberExceedException {
		if (defultDataStyleTemplate == null) {
			defultDataStyleTemplate = this.createStyle();
			defultDataStyleTemplate.setAlignment(CellStyle.ALIGN_CENTER);
		}
		return defultDataStyleTemplate;
	}

	/**
	 * @param workbook
	 * @param usedIndexsList
	 * @return
	 * @throws StyleNumberExceedException
	 */
	public CellStyleTemplate getHyperLinkStyle()
			throws StyleNumberExceedException {
		if (linkStyleTemplate == null) {
			linkStyleTemplate = this.createStyle();
			linkStyleTemplate.setAlignment(CellStyle.ALIGN_CENTER);
			linkStyleTemplate.setFillFontColor(Color.BLUE);
		}
		return linkStyleTemplate;
	}

	/**
	 * @return
	 */
	public ImageTemplate createImage() {
		return new DefaultImage();
	}

	/**
	 * @param is2007format
	 * @param path
	 * @param outputStream
	 * @throws StyleNumberExceedException
	 * @throws IOException
	 * @throws ImageNotSupportedException
	 */
	public final void createWorkbook(boolean is2007format, String path,
			OutputStream outputStream) throws StyleNumberExceedException,
			IOException {
		ExportUtils.createWorkbook(is2007format, sheetTemplates, this, path)
				.write(outputStream);
	}

	/**
	 * @param is2007format
	 * @param header
	 * @param datatList
	 * @param sheetName
	 * @param reportName
	 * @param path
	 * @param headerStyle
	 * @param dataStyle
	 * @param outputStream
	 * @param attributes
	 * @throws StyleNumberExceedException
	 * @throws IOException
	 * @throws ImageNotSupportedException
	 */
	public void createExcelsheet(boolean is2007format, List<String> header,
			List<List<String>> datatList, String sheetName, String reportName,
			String path, CellStyleTemplate headerStyle,
			CellStyleTemplate dataStyle, OutputStream outputStream,
			SheetAttribute... attributes) throws StyleNumberExceedException,
			IOException {
		createSheet(sheetName, reportName, header, datatList, attributes);
		if (headerStyle != null) {
			setDefultHeaderStyleTemplate(headerStyle);
		}
		if (dataStyle != null) {
			setDefultDataStyleTemplate(dataStyle);
		}
		createWorkbook(is2007format, path, outputStream);
	}

	/**
	 * @param startRow
	 * @param startCol
	 * @param endRow
	 * @param endCol
	 * @param value
	 * @return
	 * @throws StyleNumberExceedException
	 */
	public MergedRegion createCenteredMergedRegion(int startRow, int startCol,
			int endRow, int endCol, String value)
			throws StyleNumberExceedException {
		if (centeredStyle == null) {
			centeredStyle = createStyle();
			centeredStyle
					.setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_CENTER);
			centeredStyle
					.setVerticalAlignment(CellStyleTemplate.VERTICAL_ALIGN_CENTER);
		}
		return createMergedRegion(startRow, startCol, endRow, endCol, value,
				centeredStyle);
	}

	public MergedRegion createMergedRegion(int startRow, int startCol,
			int endRow, int endCol, String value,
			CellStyleTemplate cellStyleTemplate)
			throws StyleNumberExceedException {
		MergedRegion mergedRegion = new MergedRegion(startRow, endRow,
				startCol, endCol);
		mergedRegion.setRegionStyle(cellStyleTemplate);
		mergedRegion.setValue(value);
		return mergedRegion;
	}

	public final void setDefultHeaderStyleTemplate(
			CellStyleTemplate defultHeaderStyleTemplate) {
		this.defultHeaderStyleTemplate = defultHeaderStyleTemplate;
	}

	public final void setDefultDataStyleTemplate(
			CellStyleTemplate defultDataStyleTemplate) {
		this.defultDataStyleTemplate = defultDataStyleTemplate;
	}

	public CellStyleTemplate cloneStyleTemplate(CellStyleTemplate styleTemplate)
			throws StyleNumberExceedException {
		CellStyleTemplate cellStyleTemplate = createStyle();
		cellStyleTemplate.setAlignment(styleTemplate.getAlignment());
		cellStyleTemplate.setBorder(styleTemplate.getBorder());
		cellStyleTemplate.setBorderBottom(styleTemplate.getBorderBottom());
		cellStyleTemplate.setBorderLeft(styleTemplate.getBorderLeft());
		cellStyleTemplate.setBorderRight(styleTemplate.getBorderRight());
		cellStyleTemplate.setBorderTop(styleTemplate.getBorderTop());
		cellStyleTemplate.setFillBackgroundColor(styleTemplate
				.getFillBackgroundColor());
		cellStyleTemplate.setFillFontColor(styleTemplate.getFillFontColor());
		cellStyleTemplate.setFont(styleTemplate.getFont());
		cellStyleTemplate.setUnderLined(styleTemplate.isUnderLined());
		cellStyleTemplate.setVerticalAlignment(styleTemplate
				.getVerticalAlignment());
		cellStyleTemplate.setWrapped(styleTemplate.isWrapped());

		return cellStyleTemplate;
	}

}
