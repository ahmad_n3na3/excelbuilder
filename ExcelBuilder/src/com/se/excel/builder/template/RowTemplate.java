package com.se.excel.builder.template;

import java.util.List;



public abstract class RowTemplate {
	
	private int rowNumber = -1;
	
	public abstract void setRowHeight(float rowHeight);

	public abstract float getRowHeight();

	public abstract void setBlank(boolean isBlank);

	public abstract boolean isBlank();

	public abstract CellTemplate remove(int index);

	public abstract void add(int index, CellTemplate element);

	public abstract boolean remove(Object o);

	public abstract void setCells(List<CellTemplate> cells);

	public abstract List<CellTemplate> getCells();

	public abstract void setHeader(boolean isHeader);

	public abstract boolean isHeader();

	public abstract void setStartColumn(int startColumn);

	public abstract int getStartColumn();

	public abstract void setCellStyleTemplate(CellStyleTemplate cellStyleTemplate);

	public abstract CellStyleTemplate getCellStyleTemplate();

	public final int getRowNumber(){
		return this.rowNumber;
	}

	public final void setRowNumber(int rowNumber){
		this.rowNumber = rowNumber;
	}

}
