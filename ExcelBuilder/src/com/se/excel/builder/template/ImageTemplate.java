package com.se.excel.builder.template;


public interface ImageTemplate {
	public String getImagePath();

	public void setImagePath(String imagePath);

	public int getStartRow();

	public void setStartRow(int startRow);

	public int getEndRow();

	public void setEndRow(int endRow);

	public int getStartColumn();

	public void setStartColumn(int startColumn);

	public int getEndColumn();

	public void setEndColumn(int endColumn);

	public int getDimensionX1();

	public void setDimensionX1(int dimensionX1);

	public int getDimensionX2();

	public void setDimensionX2(int dimensionX2);

	public int getDimensionY1();

	public void setDimensionY1(int dimensionY1);

	public int getDimensionY2();

	public void setDimensionY2(int dimensionY2);

	public abstract boolean isAutoResizeable();

	public abstract void setAutoResizeable(boolean autoResizeable);

}
