package com.se.excel.builder.template;

import java.util.List;


public interface SheetTemplate {

	public abstract List<RowTemplate> getDataList();

	public abstract int getDataStartRow();

	public abstract List<ImageTemplate> getImages();

	public abstract String getReportName();

	public abstract List<RowTemplate> getSheetHeader();

	public abstract String getSheetName();

	public abstract boolean isDrawDefaultSheetHeader();

	public abstract void setDataList(List<RowTemplate> list);

	public abstract void setDataStartRow(int dataStartRow);


	public abstract void setDrawDefaultSheetHeader(boolean drawDefaultSheetHeader);

	public abstract void setImages(List<ImageTemplate> images);

	public abstract void setReportName(String reportName);

	public abstract void setSheetHeader(List<RowTemplate> sheetHeader);

	public abstract void setSheetName(String sheetName);

	public abstract List<MergedRegion> getMergedRegions();

	public abstract void setMergedRegions(List<MergedRegion> mergedRegions);

	public abstract void setReportInformation(List<SheetAttribute> reportInformation) ;

	public abstract List<SheetAttribute>  getReportInformation();

	public void setFreezPane(FreezPane freezPane);

	public FreezPane getFreezPane();

	public abstract int getDefaultColWidth();

	public abstract void setDefaultColWidth(int defaultColWidth); 
		
	

}
