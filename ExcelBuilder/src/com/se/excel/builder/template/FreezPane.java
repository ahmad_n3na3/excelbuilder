package com.se.excel.builder.template;

public class FreezPane {
	private int row;
	private int column;

	public FreezPane() {
		this(0, 0);
	}

	public FreezPane(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public final int getRow() {
		return row;
	}

	public final void setRow(int row) {
		this.row = row;
	}

	public final int getColumn() {
		return column;
	}

	public final void setColumn(int column) {
		this.column = column;
	}

}
