package com.se.excel.builder.template;

import java.awt.Color;
import java.awt.Font;

import org.apache.poi.ss.usermodel.CellStyle;

public abstract class CellStyleTemplate {

	public static final short HORIZONTAL_ALIGN_GENERAL = 0;

	public static final short HORIZONTAL_ALIGN_LEFT = 1;

	public static final short HORIZONTAL_ALIGN_CENTER = 2;

	public static final short HORIZONTAL_ALIGN_RIGHT = 3;

	public static final short HORIZONTAL_ALIGN_FILL = 4;

	public static final short HORIZONTAL_ALIGN_JUSTIFY = 5;

	public static final short VERTICAL_ALIGN_TOP = 0;

	public static final short VERTICAL_ALIGN_CENTER = 1;

	public static final short VERTICAL_ALIGN_BOTTOM = 2;

	public static final short VERTICAL_ALIGN_JUSTIFY = 3;

	public static final short BORDER_NONE = 0;

	public static final short BORDER_THIN = 1;

	public static final short BORDER_MEDIUM = 2;

	public static final short BORDER_HAIR = 4;

	public static final short BORDER_THICK = 5;

	private CellStyle cellStyle;

	public final CellStyle getCellStyle() {
		return cellStyle;
	}

	public final void setCellStyle(CellStyle cellStyle) {
		if (this.cellStyle == null) {
			this.cellStyle = cellStyle;
		}
	}

	public abstract Color getFillBackgroundColor();

	public abstract void setFillBackgroundColor(Color fillBackgroundColor);

	public abstract Color getFillFontColor();

	public abstract void setFillFontColor(Color fillFontColor);

	public abstract short getAlignment();

	public abstract void setAlignment(short alignment);

	public abstract short getVerticalAlignment();

	public abstract void setVerticalAlignment(short verticalAlignment);

	public abstract short getBorderBottom();

	public abstract void setBorderBottom(short borderBottom);

	public abstract short getBorderLeft();

	public abstract void setBorderLeft(short borderLeft);

	public abstract short getBorderRight();

	public abstract void setBorderRight(short borderRight);

	public abstract short getBorderTop();

	public abstract void setBorderTop(short borderTop);

	public abstract void setBorder(short border);

	public abstract short getBorder();

	public abstract Font getFont();

	public abstract void setFont(Font font);

	public abstract void setWrapped(boolean isWrapped);

	public abstract boolean isWrapped();

	public abstract void setUnderLined(boolean isUnderLined) ;

	public abstract boolean isUnderLined();

}
