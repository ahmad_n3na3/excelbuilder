package com.se.excel.builder.template;

public final class MergedRegion {
	private CellStyleTemplate regionStyle;
	private String value;
	private int startedRow;
	private int endedRow;
	private int startedColumun;
	private int endedColumun;

	public MergedRegion() {
		this(0, 0, 0, 0);
	}

	public MergedRegion(int startedRow, int endedRow, int startedColumun,
			int endedColumun) {
		super();
		this.startedRow = startedRow;
		this.endedRow = endedRow;
		this.startedColumun = startedColumun;
		this.endedColumun = endedColumun;
	}

	public int getStartedRow() {
		return startedRow;
	}

	public void setStartedRow(int startedRow) {
		this.startedRow = startedRow;
	}

	public int getEndedRow() {
		return endedRow;
	}

	public void setEndedRow(int endedRow) {
		this.endedRow = endedRow;
	}

	public int getStartedColumun() {
		return startedColumun;
	}

	public void setStartedColumun(int startedColumun) {
		this.startedColumun = startedColumun;
	}

	public int getEndedColumun() {
		return endedColumun;
	}

	public void setEndedColumun(int endedColumun) {
		this.endedColumun = endedColumun;
	}

	public void setRegionStyle(CellStyleTemplate regionStyle) {
		this.regionStyle = regionStyle;
	}

	public CellStyleTemplate getRegionStyle() {
		return regionStyle;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
