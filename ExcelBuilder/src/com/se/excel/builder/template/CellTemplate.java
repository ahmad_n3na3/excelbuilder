package com.se.excel.builder.template;

public interface CellTemplate {

	public CellStyleTemplate getCellStyleTemplate();

	public void setCellStyleTemplate(CellStyleTemplate cellStyleTemplate);

	public abstract String getCellValue();

	public abstract void setCellValue(String cellValue);

	public abstract boolean isLinkable();

	public abstract void setLinkable(boolean isLinkable);

	public abstract String getLinkLabel();

	public abstract void setLinkLabel(String linkLabel);

	public abstract float getCellWidth();

	public abstract void setCellWidth(float cellWidth);

	public abstract boolean isAutoResize();

	public abstract void setAutoResize(boolean isAutoResize);
}
