package com.se.excel.builder.template;

public class SheetAttribute {
	private String attributeTite;
	private String attributeData;

	public SheetAttribute() {
	this(null, null);
	}
	
	public SheetAttribute(String attributeTite, String attributeData) {
		this.attributeTite = attributeTite;
		this.attributeData = attributeData;
	}

	public final String getAttributeTite() {
		return attributeTite;
	}

	public final void setAttributeTite(String attributeTite) {
		this.attributeTite = attributeTite;
	}

	public final String getAttributeData() {
		return attributeData;
	}

	public final void setAttributeData(String attributeData) {
		this.attributeData = attributeData;
	}

}
