package com.se.excel.builder;

import java.util.List;

import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.CellTemplate;
import com.se.excel.builder.template.RowTemplate;


public class DefaultRow extends RowTemplate{
	private int startColumn;
	private CellStyleTemplate cellStyleTemplate;
	private float rowHeight;
	private boolean isBlank;
	private boolean isHeader;
	List<CellTemplate> cells;
	
	
	DefaultRow() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public CellStyleTemplate getCellStyleTemplate() {
		return cellStyleTemplate;
	}

	@Override
	public void setCellStyleTemplate(CellStyleTemplate cellStyleTemplate) {
		this.cellStyleTemplate = cellStyleTemplate;
	}

	@Override
	public int getStartColumn() {
		return startColumn;
	}

	@Override
	public void setStartColumn(int startColumn) {
		this.startColumn = startColumn;
	}

	@Override
	public boolean isHeader() {
		return isHeader;
	}

	@Override
	public void setHeader(boolean isHeader) {
		if (isBlank != true) {
			this.isHeader = isHeader;
		}
	}

	@Override
	public List<CellTemplate> getCells() {
		return cells;
	}

	@Override
	public void setCells(List<CellTemplate> cells) {
		this.cells = cells;
	}

	@Override
	public boolean remove(Object o) {
		return cells.remove(o);
	}

	@Override
	public void add(int index, CellTemplate element) {
		cells.add(index, element);
	}

	@Override
	public CellTemplate remove(int index) {
		return cells.remove(index);
	}

	@Override
	public boolean isBlank() {
		return isBlank;
	}

	@Override
	public void setBlank(boolean isBlank) {
		if (this.isHeader != true) {
			this.isBlank = isBlank;
		}

	}

	@Override
	public float getRowHeight() {
		return rowHeight;
	}

	@Override
	public void setRowHeight(float rowHeight) {
		this.rowHeight = rowHeight;
	}
}