package com.se.excel.builder.utilities;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.contrib.CellUtil;
import org.apache.poi.ss.usermodel.contrib.RegionUtil;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.se.excel.builder.ExcelBuilder;
import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.CellTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.builder.template.RowTemplate;
import com.se.excel.builder.template.SheetTemplate;
import com.se.excel.exceptions.StyleNumberExceedException;

public class ExportUtils {

	private static void writeCellsInRow(List<CellTemplate> cellTemplates,
			int startColumn, CellStyleTemplate rowStyleTemplate, Row row,
			Workbook workbook, List<Short> usedIndexsList,
			ExcelBuilder excelBuilder) throws StyleNumberExceedException {

		int column = startColumn;
		Cell cell = null;
		CellStyleTemplate styleTemplate = rowStyleTemplate != null ? rowStyleTemplate
				: excelBuilder.getDefultDatasStyleTemplate();
		if (styleTemplate.getCellStyle() == null) {
			styleTemplate.setCellStyle(DownloadUtil.createStyle(workbook,
					styleTemplate, usedIndexsList));
		}
		if (cellTemplates != null && !cellTemplates.isEmpty()) {
			for (int i = 0; i < cellTemplates.size(); i++) {
				CellStyleTemplate cellStyleTemplate = null;
				if (cellTemplates.get(i).getCellStyleTemplate() != null) {
					cellStyleTemplate = cellTemplates.get(i)
							.getCellStyleTemplate();
					if (cellStyleTemplate.getCellStyle() == null) {
						cellStyleTemplate.setCellStyle(DownloadUtil
								.createStyle(workbook, cellStyleTemplate,
										usedIndexsList));
					}
				} else {
					cellStyleTemplate = styleTemplate;
				}

				if (cellTemplates.get(i).isLinkable()) {
					CreationHelper creationHelper = workbook
							.getCreationHelper();
					Hyperlink hyperlink = creationHelper
							.createHyperlink(Hyperlink.LINK_URL);
					hyperlink.setAddress(cellTemplates.get(i).getCellValue()
							.replaceAll(" ", "%20"));
					if (excelBuilder.getHyperLinkStyle().getCellStyle() == null) {
						excelBuilder.getHyperLinkStyle().setCellStyle(
								DownloadUtil.createStyle(workbook,
										excelBuilder.getHyperLinkStyle(),
										usedIndexsList));
					}
					cell = CellUtil
							.createCell(
									row,
									column + i,
									cellTemplates.get(i).getLinkLabel(),
									cellTemplates.get(i).getCellStyleTemplate() != null
											&& cellTemplates.get(i)
													.getCellStyleTemplate()
													.getCellStyle() != null ? cellStyleTemplate
											.getCellStyle() : excelBuilder
											.getHyperLinkStyle().getCellStyle());
					cell.setHyperlink(hyperlink);

				} else {
					cell = CellUtil
							.createCell(row, column + i, cellTemplates.get(i)
									.getCellValue() != null ? cellTemplates
									.get(i).getCellValue() : "",
									cellStyleTemplate.getCellStyle());
				}
				if (cell != null && cellTemplates.get(i).getCellWidth() > 0) {
					cell.getSheet().setColumnWidth(
							cell.getColumnIndex(),
							Float.valueOf(cellTemplates.get(i).getCellWidth())
									.intValue());
				}
				if (cell != null && cellTemplates.get(i).isAutoResize()) {
					cell.getSheet().autoSizeColumn(cell.getColumnIndex());
				}
				cell = null;
			}
		}

	}

	private static void wirteRowsInSheet(List<RowTemplate> rowTemplates,
			Sheet sheet, int startDataRow, Workbook workbook,
			ExcelBuilder excelBuilder, List<Short> usedIndexsList)
			throws StyleNumberExceedException {
		// int headerRowSize = 0;
		int rowNumber = 0;
		for (int i = 0; i < rowTemplates.size(); i++) {
			rowNumber = startDataRow + i;
			if (rowTemplates.get(i).isHeader()) {
				// headerRowSize = rowTemplates.get(i).getCells() != null ?
				// rowTemplates
				// .get(i).getCells().size()
				// : 0;
				if (rowTemplates.get(i).getCellStyleTemplate() == null) {
					rowTemplates.get(i).setCellStyleTemplate(
							excelBuilder.getDefultHeaderStyleTemplate());
				}

			}
			if (rowTemplates.get(i).getCellStyleTemplate() != null
					&& rowTemplates.get(i).getCellStyleTemplate()
							.getCellStyle() == null) {
				CellStyleTemplate cellStyleTemplate = rowTemplates.get(i)
						.getCellStyleTemplate();
				cellStyleTemplate.setCellStyle(DownloadUtil.createStyle(
						workbook, cellStyleTemplate, usedIndexsList));
			}
			Row row = null;
			if (rowTemplates.get(i).getRowNumber() > -1) {
				row = sheet.getRow(rowTemplates.get(i).getRowNumber()) != null ? sheet
						.getRow(rowTemplates.get(i).getRowNumber()) : sheet
						.createRow(rowTemplates.get(i).getRowNumber());

			} else {
				row = sheet.getRow(rowNumber) != null ? sheet.getRow(rowNumber)
						: sheet.createRow(rowNumber);

			}

			if (rowTemplates.get(i).getRowHeight() > 0.0f) {
				row.setHeightInPoints(rowTemplates.get(i).getRowHeight());
			}

			if (rowTemplates.get(i).isBlank()) {
				creatBlankRow(row, rowTemplates.get(i).getCellStyleTemplate(),
						excelBuilder);
				continue;
			}

			if (rowTemplates.get(i).getCells() != null
					&& !rowTemplates.get(i).getCells().isEmpty()) {
				int size = 0;
				if (rowTemplates.get(i).getStartColumn() > 0) {
					size = rowTemplates.get(i).getStartColumn();
				}
				size += rowTemplates.get(i).getCells().size();
				if (size > excelBuilder.lastColumnWriten) {
					excelBuilder.lastColumnWriten = size;
				}
				writeCellsInRow(rowTemplates.get(i).getCells(), rowTemplates
						.get(i).getStartColumn(), rowTemplates.get(i)
						.getCellStyleTemplate(), row, workbook, usedIndexsList,
						excelBuilder);
			}

		}
	}

	private static void drawMergedRegionInSheet(
			List<MergedRegion> mergedRegions, Sheet sheet,
			List<Short> usedIndexsList) {
		for (int i = 0; i < mergedRegions.size(); i++) {

			CellRangeAddress address = new CellRangeAddress(mergedRegions
					.get(i).getStartedRow(),
					mergedRegions.get(i).getEndedRow(), mergedRegions.get(i)
							.getStartedColumun(), mergedRegions.get(i)
							.getEndedColumun());
			sheet.addMergedRegion(address);

			if (mergedRegions.get(i).getRegionStyle() != null) {
				CellStyleTemplate cellStyleTemplate = mergedRegions.get(i)
						.getRegionStyle();
				if (cellStyleTemplate.getCellStyle() == null) {
					cellStyleTemplate.setCellStyle(DownloadUtil.createStyle(
							sheet.getWorkbook(), cellStyleTemplate,
							usedIndexsList));
				}
				RegionUtil.setBorderBottom(cellStyleTemplate.getBorderBottom(),
						address, sheet, sheet.getWorkbook());
				RegionUtil.setBorderTop(cellStyleTemplate.getBorderTop(),
						address, sheet, sheet.getWorkbook());
				RegionUtil.setBorderLeft(cellStyleTemplate.getBorderLeft(),
						address, sheet, sheet.getWorkbook());
				RegionUtil.setBorderRight(cellStyleTemplate.getBorderRight(),
						address, sheet, sheet.getWorkbook());

				CellUtil.createCell(
						sheet.getRow(mergedRegions.get(i).getStartedRow()) != null ? sheet
								.getRow(mergedRegions.get(i).getStartedRow())
								: sheet.createRow(mergedRegions.get(i)
										.getStartedRow()), mergedRegions.get(i)
								.getStartedColumun(), mergedRegions.get(i)
								.getValue(), cellStyleTemplate.getCellStyle());
			}

		}
	}

	private static void drawSheetsInWorkBook(List<SheetTemplate> sheets,
			Workbook workbook, ExcelBuilder excelBuilder,
			List<Short> usedIndexsList, String pathImage)
			throws StyleNumberExceedException, IOException {
		if (sheets != null && !sheets.isEmpty()) {
			SESheetRegion region = new SESheetRegion(excelBuilder);
			for (int i = 0; i < sheets.size(); i++) {
				Sheet sheet = null;
				SheetTemplate sheetTemplate = sheets.get(i);
				if (sheetTemplate.getSheetName() != null
						&& sheetTemplate.getSheetName().trim().length() > 0) {
					sheet = workbook.createSheet(sheetTemplate.getSheetName());
				} else {
					sheet = workbook.createSheet("sheet" + (i + 1));
				}
				if (sheetTemplate.isDrawDefaultSheetHeader()) {
					region.createNewSheetHeader(sheets.get(i), excelBuilder,
							pathImage, usedIndexsList);
				}
				if (sheetTemplate.getDataList() != null
						&& !sheetTemplate.getDataList().isEmpty()) {
					wirteRowsInSheet(sheetTemplate.getDataList(), sheet,
							sheetTemplate.getDataStartRow(), workbook,
							excelBuilder, usedIndexsList);
				}

				if (sheetTemplate.getDefaultColWidth() != 0) {
					sheet.setDefaultColumnWidth(sheetTemplate
							.getDefaultColWidth());
				}

				if (sheetTemplate.getSheetHeader() != null
						&& !sheetTemplate.getSheetHeader().isEmpty()) {
					wirteRowsInSheet(sheetTemplate.getSheetHeader(), sheet, 0,
							workbook, excelBuilder, usedIndexsList);
				}

				if (sheetTemplate.getImages() != null
						&& !sheetTemplate.getImages().isEmpty()) {
					Drawing drawing = sheet.createDrawingPatriarch();
					for (int j = 0; j < sheetTemplate.getImages().size(); j++) {
						ImageHelperUtils.addPictureToSheet(workbook, drawing,
								sheetTemplate.getImages().get(j));
					}
				}

				if (sheetTemplate.getMergedRegions() != null
						&& !sheetTemplate.getMergedRegions().isEmpty()) {
					drawMergedRegionInSheet(sheetTemplate.getMergedRegions(),
							sheet, usedIndexsList);
				}

				if (sheetTemplate.getFreezPane() != null
						&& (sheetTemplate.getFreezPane().getColumn() != 0 || sheetTemplate
								.getFreezPane().getRow() != 0)) {
					if (sheetTemplate.getFreezPane().getRow() != 0
							&& sheetTemplate.getFreezPane().getColumn() != 0) {
						sheet.createFreezePane(sheetTemplate.getFreezPane()
								.getColumn(), sheetTemplate.getFreezPane()
								.getRow());
					} else if (sheetTemplate.getFreezPane().getColumn() != 0) {
						sheet.createFreezePane(sheetTemplate.getFreezPane()
								.getColumn(), 0, sheetTemplate.getFreezPane()
								.getColumn(), 0);
					} else {
						sheet.createFreezePane(0, sheetTemplate.getFreezPane()
								.getRow(), 0, sheetTemplate.getFreezPane()
								.getRow());
					}

				}
				sheet.setColumnBreak(6);
			}
		}
	}

	public static Workbook createWorkbook(boolean is2007format,
			List<SheetTemplate> sheetTemplates, ExcelBuilder excelBuilder,
			String path) throws StyleNumberExceedException, IOException {
		List<Short> usedIndexsList = new ArrayList<Short>();
		usedIndexsList.add(Short.valueOf(IndexedColors.BLACK.getIndex()));
		usedIndexsList.add(Short.valueOf(IndexedColors.BLUE.getIndex()));
		usedIndexsList.add(Short.valueOf(IndexedColors.RED.getIndex()));
		usedIndexsList.add(Short.valueOf(IndexedColors.GREEN.getIndex()));
		usedIndexsList.add(Short.valueOf(IndexedColors.YELLOW.getIndex()));
		usedIndexsList.add(Short.valueOf(IndexedColors.WHITE.getIndex()));
		Workbook workbook = null;
		excelBuilder.lastColumnWriten = 5;

		if (is2007format) {
			workbook = new XSSFWorkbook();
		} else {
			workbook = new HSSFWorkbook();
		}
		drawSheetsInWorkBook(sheetTemplates, workbook, excelBuilder,
				usedIndexsList, path);
		return workbook;
	}

	private static void creatBlankRow(Row row,
			CellStyleTemplate cellStyleTemplate, ExcelBuilder builder) {
		if (cellStyleTemplate == null) {
			return;
		}
		for (int i = 0; i < builder.lastColumnWriten; i++) {
			Cell cell = row.createCell(i);
			cell.setCellStyle(cellStyleTemplate.getCellStyle());
		}
	}
}
