package com.se.excel.builder.utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.se.excel.builder.template.ImageTemplate;


public class ImageHelperUtils {
	/**
	 * Creates a temp file on server to load the into it and add it into the
	 * current workbook
	 * 
	 * @param imageFile
	 *            path of image to be load
	 * @param wb
	 *            current workbook that the picture will added to
	 * @return index of the picture in the workbook
	 * @author Ahmad Neanaa
	 * @throws IOException
	 */
	private static int loadPicture(final String imageFile, final Workbook wb) throws IOException{
		// logger.info(imageFile);
		int pictureIndex = 0;
			final File f = File.createTempFile("" + System.nanoTime(), ""
					+ System.nanoTime()+".png");
			if (imageFile.contains("http")) {
				FileUtils.copyURLToFile(new URL(imageFile), f);
			} else {
				FileUtils.copyFile(new File(imageFile), f);
			}
			pictureIndex = wb.addPicture(FileUtils.readFileToByteArray(f),
					Workbook.PICTURE_TYPE_PNG);
			

			System.out.println("**************************" + pictureIndex);
		return pictureIndex;
	}

	/**
	 * put the picture added in workbook in a specific position in <br/>
	 * the specified sheet at specified column and row rang
	 * 
	 * @param workbook
	 *            current {@link Workbook} that will have the picture
	 * @param sheetName
	 *            the specified sheet name
	 * @return
	 * @author Ahmad Neanaa
	 * @throws IOException
	 */

	public static Picture addPictureToSheet(final Workbook workbook,
			final Drawing drawingPatriarch, ImageTemplate imageTemplate){

		Picture picture = null;

		try{
		CreationHelper helper = workbook.getCreationHelper();

		ClientAnchor anchor = helper.createClientAnchor();

		anchor.setCol1(imageTemplate.getStartColumn());
		anchor.setRow1(imageTemplate.getStartRow());
		if (!imageTemplate.isAutoResizeable()) {
			anchor.setCol2(imageTemplate.getEndColumn());
			anchor.setRow2(imageTemplate.getEndRow());
			anchor.setDx1(imageTemplate.getDimensionX1());
			anchor.setDy1(imageTemplate.getDimensionY1());
			anchor.setDx2(imageTemplate.getDimensionX2());
			anchor.setDy2(imageTemplate.getDimensionY2());
		}
		
			picture = drawingPatriarch.createPicture(anchor,
					loadPicture(imageTemplate.getImagePath(), workbook));

		
		if (imageTemplate.isAutoResizeable()) {
			picture.resize();
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return picture;
	}
}
