package com.se.excel.builder.utilities;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import com.se.excel.builder.ExcelBuilder;
import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.CellTemplate;
import com.se.excel.builder.template.ImageTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.builder.template.RowTemplate;
import com.se.excel.builder.template.SheetAttribute;
import com.se.excel.builder.template.SheetTemplate;
import com.se.excel.exceptions.StyleNumberExceedException;

public class SESheetRegion {

	CellStyleTemplate firstRowCellStyleTemplate;
	CellStyleTemplate secRowCellStyleTemplate;
	CellStyleTemplate informationCellTitle;
	CellStyleTemplate informationCellData;
	CellStyleTemplate speratorCellStyleTemplate;

	public SESheetRegion(ExcelBuilder excelBuilder)
			throws StyleNumberExceedException {
		firstRowCellStyleTemplate = excelBuilder.createStyle();
		firstRowCellStyleTemplate.setBorder(CellStyleTemplate.BORDER_NONE);
		firstRowCellStyleTemplate
				.setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_LEFT);

		secRowCellStyleTemplate = excelBuilder.createStyle();
		secRowCellStyleTemplate.setBorder(CellStyleTemplate.BORDER_NONE);
		secRowCellStyleTemplate
				.setFillBackgroundColor(new Color(242, 242, 242));
		secRowCellStyleTemplate
				.setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_LEFT);
		secRowCellStyleTemplate.setFont(new Font("Calibri", Font.BOLD, 16));
		secRowCellStyleTemplate
				.setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_LEFT);

		// informationCellTitle = excelBuilder.createStyle();
		// informationCellTitle.setBorder(CellStyleTemplate.BORDER_NONE);
		// informationCellTitle.setFont(new Font("Calibri", Font.BOLD, 11));
		// informationCellTitle
		// .setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_LEFT);

		informationCellData = excelBuilder.createStyle();
		informationCellData.setBorder(CellStyleTemplate.BORDER_NONE);
		informationCellData.setFont(new Font("Calibri", Font.PLAIN, 11));
		informationCellData
				.setAlignment(CellStyleTemplate.HORIZONTAL_ALIGN_LEFT);
		speratorCellStyleTemplate = excelBuilder.createStyle();
		speratorCellStyleTemplate
				.setFillBackgroundColor(new Color(83, 141, 213));
		speratorCellStyleTemplate.setBorder(CellStyleTemplate.BORDER_NONE);
	}

	/**
	 *
	 */
	//
	// public void createSheetHeader(final SheetTemplate sheetTemplate,
	// ExcelBuilder builder) throws StyleNumberExceedException {
	//
	// ExcelBuilder excelBuilder = builder;
	//
	// java.awt.Font font = new java.awt.Font("Calibri", java.awt.Font.BOLD,
	// 14);
	// Color fontColor = new Color(255, 255, 255);
	//
	// java.awt.Font dataFont = new java.awt.Font("Calibri",
	// java.awt.Font.BOLD, 10);
	//
	// CellStyleTemplate contentHeaderCellStyleTemplate = excelBuilder
	// .createStyle();
	// contentHeaderCellStyleTemplate.setFillBackgroundColor(Color.BLACK);
	// contentHeaderCellStyleTemplate.setFont(font);
	// contentHeaderCellStyleTemplate.setFillFontColor(fontColor);
	//
	// CellStyleTemplate contentdataCellStyleTemplate = excelBuilder
	// .createStyle();
	// contentdataCellStyleTemplate.setFont(dataFont);
	//
	// List<RowTemplate> rowTemplates = new ArrayList<RowTemplate>();
	//
	// List<MergedRegion> mergedRegions = new ArrayList<MergedRegion>();
	//
	// MergedRegion mergedRegion = new MergedRegion(0, 0, 4, 11);
	//
	// mergedRegions.add(mergedRegion);
	//
	// RowTemplate headerRowTemplate = excelBuilder.createRow(4);
	// headerRowTemplate.setCellStyleTemplate(contentHeaderCellStyleTemplate);
	// List<CellTemplate> cellTemplates = excelBuilder
	// .createCells("SiliconExpert Technologies Inc");
	// headerRowTemplate.setCells(cellTemplates);
	// rowTemplates.add(headerRowTemplate);
	//
	// // sheet.addMergedRegion(new CellRangeAddress);
	//
	// // riskReport row
	// RowTemplate reportNameRowTemplate = excelBuilder.createRow(4);
	// reportNameRowTemplate.setRowHeight(20);
	// reportNameRowTemplate
	// .setCellStyleTemplate(contentHeaderCellStyleTemplate);
	// List<CellTemplate> reportNameCells = excelBuilder
	// .createCells(sheetTemplate.getReportName());
	// reportNameRowTemplate.setCells(reportNameCells);
	// rowTemplates.add(reportNameRowTemplate);
	//
	// mergedRegion = new MergedRegion(1, 1, 4, 11);
	// mergedRegions.add(mergedRegion);
	//
	// // sheet.addMergedRegion(new CellRangeAddress(1, 1, 4, 11));
	// // empty row
	//
	// //
	// ---------------------------------------------------------------------------
	//
	// rowTemplates.add(excelBuilder.createBlankRow());
	// // User Name row:
	// // RowTemplate userNameRowTemplate = excelBuilder.createRow();
	// // List<CellTemplate> userNameCells = excelBuilder.createCells(
	// // "User Name", sheetTemplate.getUserName());
	// //
	// userNameRowTemplate.setCellStyleTemplate(contentdataCellStyleTemplate);
	// // userNameRowTemplate.setCells(userNameCells);
	// // rowTemplates.add(userNameRowTemplate);
	//
	// mergedRegion = new MergedRegion(3, 3, 1, 2);
	// mergedRegion.setRegionStyle(contentdataCellStyleTemplate);
	// mergedRegions.add(mergedRegion);
	//
	// RowTemplate dateRowTemplate = excelBuilder.createRow();
	// dateRowTemplate.setCellStyleTemplate(contentdataCellStyleTemplate);
	// List<CellTemplate> dateCells = excelBuilder.createCells("Date",
	// DownloadUtil.getDefultDateFormat(new Date()));
	// dateRowTemplate.setCells(dateCells);
	// rowTemplates.add(dateRowTemplate);
	//
	// mergedRegion = new MergedRegion(4, 4, 1, 2);
	// mergedRegion.setRegionStyle(contentdataCellStyleTemplate);
	// mergedRegions.add(mergedRegion);
	//
	// sheetTemplate.setSheetHeader(rowTemplates);
	// sheetTemplate.setMergedRegions(mergedRegions);
	// }

	public void createNewSheetHeader(SheetTemplate sheetTemplate,
			ExcelBuilder excelBuilder, String path, List<Short> usedIndexsList)
			throws StyleNumberExceedException {
		ExcelBuilder builder = excelBuilder;

		List<ImageTemplate> imageTemplates = new ArrayList<ImageTemplate>();
		List<RowTemplate> rowTemplates = new ArrayList<RowTemplate>();

		RowTemplate firstRowTemplate = builder.createBlankRow();
		firstRowTemplate.setCellStyleTemplate(firstRowCellStyleTemplate);
		firstRowTemplate.setRowHeight(12.75f);
		rowTemplates.add(firstRowTemplate);

		RowTemplate secRowTemplate = builder.createRow();
		secRowTemplate.setCellStyleTemplate(secRowCellStyleTemplate);
		secRowTemplate.setRowHeight(21.00f);
		List<CellTemplate> secCellTemplates = builder.createCells("", "");

		secCellTemplates.get(0).setCellStyleTemplate(firstRowCellStyleTemplate);
		secCellTemplates.get(1).setCellStyleTemplate(firstRowCellStyleTemplate);
		secRowTemplate.setCells(secCellTemplates);
		rowTemplates.add(secRowTemplate);

		if (sheetTemplate.getMergedRegions() == null) {
			sheetTemplate.setMergedRegions(new ArrayList<MergedRegion>());
		}

		MergedRegion mergedRegion = new MergedRegion(1, 1, 2, 3);
		mergedRegion.setValue(sheetTemplate.getReportName());
		mergedRegion.setRegionStyle(secRowCellStyleTemplate);

		sheetTemplate.getMergedRegions().add(mergedRegion);

		ImageTemplate imageTemplate = builder.createImage();
		imageTemplate.setImagePath(path);
		imageTemplate.setEndColumn(1);
		imageTemplate.setEndRow(2);
		
		imageTemplates.add(imageTemplate);

		RowTemplate informationRow = null;

		if (sheetTemplate.getReportInformation() != null
				&& !sheetTemplate.getReportInformation().isEmpty()) {
			List<SheetAttribute> sheetAttributes = sheetTemplate
					.getReportInformation();
			for (int i = 0; i < sheetAttributes.size(); i++) {
				informationRow = builder.createRow();
				informationRow.setCellStyleTemplate(firstRowCellStyleTemplate);
				List<CellTemplate> attributeCells = null;
				if ((i + 1 == sheetAttributes.size())
						|| sheetAttributes.size() == 1) {
					attributeCells = builder
							.createCells(
									"",
									"",
									(sheetAttributes.get(i).getAttributeTite() != null
											&& !sheetAttributes.get(i)
													.getAttributeTite()
													.isEmpty() ? sheetAttributes
											.get(i).getAttributeTite() + " : "
											: "")
											+ ""
											+ sheetAttributes.get(i)
													.getAttributeData(), "");
					attributeCells.get(2).setCellStyleTemplate(
							informationCellData);
				} else {

					attributeCells = builder
							.createCells(
									"",
									"",
									(sheetAttributes.get(i).getAttributeTite() != null
											&& !sheetAttributes.get(i)
													.getAttributeTite()
													.isEmpty() ? sheetAttributes
											.get(i).getAttributeTite() + " : "
											: "")
											+ ""
											+ sheetAttributes.get(i)
													.getAttributeData(),
									(sheetAttributes.get(i).getAttributeTite() != null
											&& !sheetAttributes.get(i + 1)
													.getAttributeTite()
													.isEmpty() ? sheetAttributes
											.get(i + 1).getAttributeTite()
											+ " : " : "")
											+ ""
											+ sheetAttributes.get(i + 1)
													.getAttributeData());
					attributeCells.get(2).setCellStyleTemplate(
							informationCellData);
					attributeCells.get(3).setCellStyleTemplate(
							informationCellData);
					i++;
				}

				informationRow.setCells(attributeCells);
				rowTemplates.add(informationRow);
			}
		}

		RowTemplate speratorRow = builder.createBlankRow();
		speratorRow.setCellStyleTemplate(speratorCellStyleTemplate);
		speratorRow.setRowHeight(4.5f);
		informationRow = builder.createBlankRow();
		informationRow.setCellStyleTemplate(firstRowCellStyleTemplate);
		rowTemplates.add(informationRow);
		rowTemplates.add(speratorRow);
		sheetTemplate.setSheetHeader(rowTemplates);
		if (sheetTemplate.getImages() == null
				|| sheetTemplate.getImages().isEmpty()) {
			sheetTemplate.setImages(imageTemplates);
		} else {
			sheetTemplate.getImages().addAll(imageTemplates);
		}
		if (rowTemplates.size() > sheetTemplate.getDataStartRow()) {
			sheetTemplate.setDataStartRow(rowTemplates.size());
		}

	}
}
