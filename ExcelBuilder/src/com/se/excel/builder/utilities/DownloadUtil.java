package com.se.excel.builder.utilities;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.se.excel.builder.template.CellStyleTemplate;

public class DownloadUtil {

	public static CellStyle createStyle(Workbook workbook,
			CellStyleTemplate styleTemplate, List<Short> usedIndexsList) {
		final CellStyle contentMainHeaderCellStyle = workbook.createCellStyle();
		if (styleTemplate != null) {
			if (styleTemplate.getFillBackgroundColor() != null) {
				int red = styleTemplate.getFillBackgroundColor().getRed();
				int blue = styleTemplate.getFillBackgroundColor().getBlue();
				int green = styleTemplate.getFillBackgroundColor().getGreen();

				if (red == 0 && blue == 0 && green == 0) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.BLACK
									.getIndex());
				} else if (red == 255 && blue == 0 && green == 0) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.RED
									.getIndex());
				} else if (red == 0 && blue == 255 && green == 0) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.BLUE
									.getIndex());
				} else if (red == 0 && blue == 0 && green == 255) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.GREEN
									.getIndex());
				} else if (red == 255 && blue == 0 && green == 255) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.YELLOW
									.getIndex());
				} else if (red == 255 && blue == 255 && green == 255) {
					contentMainHeaderCellStyle
							.setFillForegroundColor(IndexedColors.WHITE
									.getIndex());
				} else {
					creatFillForegroundColor(contentMainHeaderCellStyle,
							workbook, red, green, blue, usedIndexsList);
				}

			} else {
				contentMainHeaderCellStyle
						.setFillForegroundColor(IndexedColors.BLACK.getIndex());
			}

			contentMainHeaderCellStyle
					.setFillPattern(CellStyle.SOLID_FOREGROUND);
			contentMainHeaderCellStyle.setAlignment(styleTemplate
					.getAlignment());
			contentMainHeaderCellStyle.setBorderBottom(styleTemplate
					.getBorderBottom());
			contentMainHeaderCellStyle.setBorderLeft(styleTemplate
					.getBorderLeft());
			contentMainHeaderCellStyle.setBorderRight(styleTemplate
					.getBorderRight());
			contentMainHeaderCellStyle.setBorderTop(styleTemplate
					.getBorderTop());
			contentMainHeaderCellStyle.setFont(createFont(workbook,
					styleTemplate.getFont(), styleTemplate.getFillFontColor(),
					styleTemplate.isUnderLined(), usedIndexsList));
			contentMainHeaderCellStyle.setVerticalAlignment(styleTemplate
					.getVerticalAlignment());
			contentMainHeaderCellStyle.setWrapText(styleTemplate.isWrapped());
			return contentMainHeaderCellStyle;
		}
		return null;
	}

	public static Font createFont(Workbook workbook, java.awt.Font font,
			Color fontColor, boolean isUnderLined, List<Short> usedIndexsList) {
		Font contentMainHeaderFont = workbook.createFont();
		if (font == null) {
			contentMainHeaderFont.setFontHeightInPoints((short) 10);
			contentMainHeaderFont.setFontName("Calibri");
			contentMainHeaderFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		} else {
			contentMainHeaderFont.setFontHeightInPoints((short) font.getSize());
			contentMainHeaderFont.setFontName(font.getFamily());
			switch (font.getStyle()) {
			case java.awt.Font.BOLD:
				contentMainHeaderFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
				break;
			case java.awt.Font.PLAIN:
				contentMainHeaderFont.setBoldweight(Font.BOLDWEIGHT_NORMAL);
				break;
			default:
				contentMainHeaderFont.setBoldweight(Font.BOLDWEIGHT_NORMAL);
			}
		}

		if (isUnderLined) {
			contentMainHeaderFont.setUnderline(Font.U_SINGLE);
		}
		if (fontColor != null) {
			int red = fontColor.getRed();
			int blue = fontColor.getBlue();
			int green = fontColor.getGreen();

			if (red == 0 && blue == 0 && green == 0) {
				contentMainHeaderFont.setColor(IndexedColors.BLACK.getIndex());
			} else if (red == 255 && blue == 0 && green == 0) {
				contentMainHeaderFont.setColor(IndexedColors.RED.getIndex());
			} else if (red == 0 && blue == 255 && green == 0) {
				contentMainHeaderFont.setColor(IndexedColors.BLUE.getIndex());
			} else if (red == 0 && blue == 0 && green == 255) {
				contentMainHeaderFont.setColor(IndexedColors.GREEN.getIndex());
			} else if (red == 255 && blue == 0 && green == 255) {
				contentMainHeaderFont.setColor(IndexedColors.YELLOW.getIndex());
			} else if (red == 255 && blue == 255 && green == 255) {
				contentMainHeaderFont.setColor(IndexedColors.WHITE.getIndex());
			} else {
				contentMainHeaderFont = creatFontColor(contentMainHeaderFont,
						workbook, red, green, blue, usedIndexsList);
			}

		} else {
			contentMainHeaderFont.setColor(IndexedColors.WHITE.getIndex());
		}
		return contentMainHeaderFont;
	}

	public static String getMonth(final int monthValue) {
		if (monthValue == 1) {
			return "January";
		} else if (monthValue == 2) {
			return "February";
		} else if (monthValue == 3) {
			return "March";
		} else if (monthValue == 4) {
			return "April";
		} else if (monthValue == 5) {
			return "May";
		} else if (monthValue == 6) {
			return "June";
		} else if (monthValue == 7) {
			return "July";
		} else if (monthValue == 8) {
			return "August";
		} else if (monthValue == 9) {
			return "September";
		} else if (monthValue == 10) {
			return "October";
		} else if (monthValue == 11) {
			return "November";
		} else if (monthValue == 12) {
			return "December";
		}

		return "";
	}

	public static void insertDataInCell(final Sheet sheet, final int cell,
			final int rowNumber, final CellStyle contentDataCellStyle,
			final String data) {
		Row row = sheet.getRow(rowNumber);
		if (row == null)
			row = sheet.createRow(rowNumber);
		final Cell seLifecycle = row.createCell((short) (cell));

		if (data != null) {
			if ((data.equals("null"))) {
				seLifecycle.setCellValue("");
			} else {
				seLifecycle.setCellValue(data);

			}
		} else {
			seLifecycle.setCellValue("");
		}

		seLifecycle.setCellStyle(contentDataCellStyle);
	}

	public static String getDefultDateFormat(Date dateStr) {
		if (dateStr != null) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MMMM dd,yyyy - hh:mm");
				return dateFormat.format(dateStr);
			} catch (Exception e) {
				e.printStackTrace();
				return "";
			}
		}
		return "";
	}

	public static String getDefultDateFormatSec(Date dateStr) {
		if (dateStr != null) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"MMMM dd,yyyy - hh:mm:ss aaa");
				return dateFormat.format(dateStr);
			} catch (Exception e) {
				e.printStackTrace();
				return "";
			}
		}
		return "";
	}

	public static String getDefultDateFormatUsageReport(Date dateStr) {
		if (dateStr != null) {
			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				return dateFormat.format(dateStr);
			} catch (Exception e) {
				e.printStackTrace();
				return "";
			}
		}
		return "";
	}

	public static int check60000Rows(int arrayListSize) {
		if (arrayListSize > 60000)
			return 60000;
		return arrayListSize;
	}

	/**
	 * Creates custom color for foreground color
	 * 
	 * @param cellStyle
	 *            cell style to altered
	 * @param workbook
	 *            current work book
	 * @param red
	 *            RGP red value
	 * @param green
	 *            RGP green value
	 * @param blue
	 *            RGP blue
	 * @param usedIndexsList
	 * @return altered {@link CellStyle}
	 */
	private static CellStyle creatFillForegroundColor(
			final CellStyle cellStyle, final Workbook workbook, int red,
			int green, int blue, List<Short> usedIndexsList) {
		if (workbook instanceof HSSFWorkbook) {
			HSSFWorkbook hssfWorkbook = (HSSFWorkbook) workbook;
			short colorIndex = getNotUsedColorIndex(usedIndexsList);
			hssfWorkbook.getCustomPalette().setColorAtIndex(colorIndex,
					(byte) red, (byte) green, (byte) blue);

			HSSFCellStyle hssfCellStyle = (HSSFCellStyle) cellStyle;
			hssfCellStyle.setFillForegroundColor(colorIndex);

			return hssfCellStyle;
		} else if (workbook instanceof XSSFWorkbook) {
			XSSFCellStyle xssfCellStyle = (XSSFCellStyle) cellStyle;
			xssfCellStyle.setFillForegroundColor(new XSSFColor(new Color(red,
					green, blue)));
			return xssfCellStyle;
		}
		return cellStyle;
	}

	/**
	 * Creates custom color for font color
	 * 
	 * @param cellStyle
	 *            cell style to altered
	 * @param workbook
	 *            current work book
	 * @param red
	 *            RGP red value
	 * @param green
	 *            RGP green value
	 * @param blue
	 *            RGP blue
	 * @param usedIndexsList
	 * @return altered {@link CellStyle}
	 */
	private static Font creatFontColor(final Font font,
			final Workbook workbook, int red, int green, int blue,
			List<Short> usedIndexsList) {
		if (workbook instanceof HSSFWorkbook) {
			HSSFWorkbook hssfWorkbook = (HSSFWorkbook) workbook;
			short colorIndex = getNotUsedColorIndex(usedIndexsList);
			hssfWorkbook.getCustomPalette().setColorAtIndex(colorIndex,
					(byte) red, (byte) green, (byte) blue);

			HSSFFont hssfFont = (HSSFFont) font;
			hssfFont.setColor(colorIndex);

			return hssfFont;
		} else if (workbook instanceof XSSFWorkbook) {
			XSSFFont xssfFont = (XSSFFont) font;
			xssfFont.setColor(new XSSFColor(new Color(red, green, blue)));
			return xssfFont;
		}
		return font;
	}

	public static short getNotUsedColorIndex(List<Short> usedIndexedColors) {
		short index = 0;

		IndexedColors[] color = IndexedColors.values();
		for (int i = 0; i < color.length; i++) {
			if (!usedIndexedColors.contains(Short.valueOf(color[i].getIndex()))) {
				index = color[i].getIndex();
				usedIndexedColors.add(Short.valueOf(index));
				break;
			}
		}

		return index;
	}

	/**
	 * @author ahmad_saleh
	 * @param startRow
	 * @param endRow
	 * @param startCell
	 * @param endCell
	 * @param sheet
	 * @param contentDataCellStyle
	 */
	public static void margedCell(int startRow, int endRow, short startCell,
			short endCell, Sheet sheet, CellStyle contentDataCellStyle) {

		// check Rows isCreated
		for (int i = (startRow); i < (endRow); i++) {
			Row row = sheet.getRow(i);
			if (row == null) {
				row = sheet.createRow((short) i);
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(startRow, endRow, startCell,
				endCell));

		// / add style to Cell
		for (int j = (startRow); j < (endRow); j++) {
			for (int i = (startCell); i < (endCell); i++) {

				final Cell cell = sheet.getRow(j).createCell((short) i);
				cell.setCellStyle(contentDataCellStyle);
			}
		}

	}
}
