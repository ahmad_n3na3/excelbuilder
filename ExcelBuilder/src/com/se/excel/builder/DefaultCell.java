package com.se.excel.builder;

import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.CellTemplate;

public class DefaultCell implements CellTemplate {
	private boolean isLinkable;
	private String linkLabel;
	private String cellValue;
	private float cellWidth = -1;
	private boolean isAutoResize;
	private CellStyleTemplate cellStyleTemplate;

	DefaultCell() {
		this(null);
	}

	DefaultCell(String cellValue) {
		super();
		this.cellValue = cellValue;
	}

	public CellStyleTemplate getCellStyleTemplate() {
		return this.cellStyleTemplate;
	}

	public void setCellStyleTemplate(CellStyleTemplate cellStyleTemplate) {
		this.cellStyleTemplate = cellStyleTemplate;
	}

	public String getCellValue() {
		return cellValue;
	}

	public void setCellValue(String cellValue) {
		this.cellValue = cellValue;
	}

	public boolean isLinkable() {
		return isLinkable;
	}

	public void setLinkable(boolean isLinkable) {
		this.isLinkable = isLinkable;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public float getCellWidth() {
		return this.cellWidth;
	}

	public void setCellWidth(float cellWidth) {
		this.cellWidth = cellWidth;
	}

	@Override
	public void setAutoResize(boolean isAutoResize) {
		this.isAutoResize = isAutoResize;
	}

	@Override
	public boolean isAutoResize() {
		return isAutoResize;
	}

}
