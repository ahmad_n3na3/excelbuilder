package com.se.excel.builder;

import com.se.excel.builder.template.ImageTemplate;

/**
 * 
 * @author ahmad_saleh
 * 
 */
public class DefaultImage implements ImageTemplate {
	private String imagePath;
	private int startRow = 0;
	private int endRow;
	private int startColumn = 0;
	private int endColumn;
	private int DimensionX1;
	private int DimensionX2;
	private int DimensionY1;
	private int DimensionY2;
	private boolean autoResizeable;

	DefaultImage() {
		// TODO Auto-generated constructor stub
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath){
		this.imagePath = imagePath;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public void setStartColumn(int startColumn) {
		this.startColumn = startColumn;
	}

	public int getEndColumn() {
		return endColumn;
	}

	public void setEndColumn(int endColumn) {
		this.endColumn = endColumn;
	}

	public int getDimensionX1() {
		return DimensionX1;
	}

	public void setDimensionX1(int dimensionX1) {
		DimensionX1 = dimensionX1;
	}

	public int getDimensionX2() {
		return DimensionX2;
	}

	public void setDimensionX2(int dimensionX2) {
		DimensionX2 = dimensionX2;
	}

	public int getDimensionY1() {
		return DimensionY1;
	}

	public void setDimensionY1(int dimensionY1) {
		DimensionY1 = dimensionY1;
	}

	public int getDimensionY2() {
		return DimensionY2;
	}

	public void setDimensionY2(int dimensionY2) {
		DimensionY2 = dimensionY2;
	}

	@Override
	public void setAutoResizeable(boolean autoResizeable) {
		this.autoResizeable = autoResizeable;
	}

	@Override
	public boolean isAutoResizeable() {
		return autoResizeable;
	}

}
