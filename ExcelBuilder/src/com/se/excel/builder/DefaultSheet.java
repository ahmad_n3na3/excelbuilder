package com.se.excel.builder;

import java.util.List;

import com.se.excel.builder.template.FreezPane;
import com.se.excel.builder.template.ImageTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.builder.template.RowTemplate;
import com.se.excel.builder.template.SheetAttribute;
import com.se.excel.builder.template.SheetTemplate;


public class DefaultSheet implements SheetTemplate {

	private int dataStartRow = 5;
	private String sheetName;
	private String reportName; 
	private List<RowTemplate> sheetHeader;
	private List<RowTemplate> list;
	private List<ImageTemplate> images;
	private boolean drawDefaultSheetHeader = true;
	private List<MergedRegion> mergedRegions;
	private List<SheetAttribute> attributes;
	private FreezPane freezPane;
	private int defaultColWidth = 40;
	
	DefaultSheet() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getSheetName() {
		return sheetName;
	}

	@Override
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	@Override
	public String getReportName() {
		return reportName;
	}

	@Override
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	@Override
	public List<RowTemplate> getDataList() {
		return list;
	}

	@Override
	public void setDataList(List<RowTemplate> list) {
		this.list = list;
	}

	@Override
	public int getDataStartRow() {
		return dataStartRow;
	}

	@Override
	public void setDataStartRow(int dataStartRow) {
		this.dataStartRow = dataStartRow;
	}

	@Override
	public void setImages(List<ImageTemplate> images) {
		this.images = images;
	}

	@Override
	public List<ImageTemplate> getImages() {
		return images;
	}

	@Override
	public void setDrawDefaultSheetHeader(boolean drawDefaultSheetHeader) {
		this.drawDefaultSheetHeader = drawDefaultSheetHeader;
	}

	@Override
	public boolean isDrawDefaultSheetHeader() {
		return drawDefaultSheetHeader;
	}

	@Override
	public void setSheetHeader(List<RowTemplate> sheetHeader) {
		this.sheetHeader = sheetHeader;
	}

	@Override
	public List<RowTemplate> getSheetHeader() {
		return sheetHeader;
	}

	@Override
	public void setMergedRegions(List<MergedRegion> mergedRegions) {
		this.mergedRegions = mergedRegions;
	}

	@Override
	public List<MergedRegion> getMergedRegions() {
		return mergedRegions;
	}
	@Override
	public void setReportInformation(List<SheetAttribute> reportInformation) {
		this.attributes = reportInformation;
		
	}

	@Override
	public List<SheetAttribute> getReportInformation() {
		// TODO Auto-generated method stub
		return this.attributes;
	}

	@Override
	public void setFreezPane(FreezPane freezPane) {
		this.freezPane = freezPane;
		
	}

	@Override
	public FreezPane getFreezPane() {
		return this.freezPane;
	}

	@Override
	public void setDefaultColWidth(int defaultColWidth) {
		this.defaultColWidth = defaultColWidth;
	}

	@Override
	public int getDefaultColWidth() {
		return defaultColWidth;
	}
}
