package com.se.excel.reader.template.impl;

import java.awt.Color;
import java.awt.Font;

import com.se.excel.builder.template.CellStyleTemplate;

public class CellStyleTemplateImpl extends CellStyleTemplate {
	private Color fillBackgroundColor;
	private Color fillFontColor;
	private short alignment;
	private short verticalAlignment;
	private short borderBottom;
	private short borderLeft;
	private short borderRight;
	private short borderTop;
	private short border;
	private Font font;
	private boolean isWrapped;
	private boolean isUnderLined;

	@Override
	public Color getFillBackgroundColor() {

		return this.fillBackgroundColor;
	}

	@Override
	public void setFillBackgroundColor(Color fillBackgroundColor) {
		this.fillBackgroundColor = fillBackgroundColor;

	}

	@Override
	public Color getFillFontColor() {
		return this.fillFontColor;
	}

	@Override
	public void setFillFontColor(Color fillFontColor) {
		this.fillFontColor = fillFontColor;

	}

	@Override
	public short getAlignment() {
		return this.alignment;
	}

	@Override
	public void setAlignment(short alignment) {
		this.alignment = alignment;

	}

	@Override
	public short getVerticalAlignment() {
		return this.verticalAlignment;
	}

	@Override
	public void setVerticalAlignment(short verticalAlignment) {
		this.verticalAlignment = verticalAlignment;
	}

	@Override
	public short getBorderBottom() {
		return this.borderBottom;
	}

	@Override
	public void setBorderBottom(short borderBottom) {
		this.borderBottom = borderBottom;
	}

	@Override
	public short getBorderLeft() {
		return this.borderLeft;
	}

	@Override
	public void setBorderLeft(short borderLeft) {
		this.borderLeft = borderLeft;

	}

	@Override
	public short getBorderRight() {
		return this.borderRight;
	}

	@Override
	public void setBorderRight(short borderRight) {
		this.borderRight = borderRight;
	}

	@Override
	public short getBorderTop() {
		return this.borderTop;
	}

	@Override
	public void setBorderTop(short borderTop) {
		this.borderTop = borderTop;
	}

	@Override
	public void setBorder(short border) {
		this.border = border;
		this.borderBottom = border;
		this.borderTop = border;
		this.borderLeft = border;
		this.borderRight = border;

	}

	@Override
	public short getBorder() {
		return this.border;
	}

	@Override
	public Font getFont() {
		return this.font;
	}

	@Override
	public void setFont(Font font) {
		this.font = font;

	}

	@Override
	public void setWrapped(boolean isWrapped) {
		this.isWrapped = isWrapped;

	}

	@Override
	public boolean isWrapped() {
		return this.isWrapped;
	}

	@Override
	public void setUnderLined(boolean isUnderLined) {
		this.isUnderLined = isUnderLined;
		
	}

	@Override
	public boolean isUnderLined() {
		// TODO Auto-generated method stub
		return this.isUnderLined;
	}

}
