package com.se.excel.reader.utilities;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ReadExcelFile {
	// =====================================================================================
	public static List<List<String>> readExcelFile(Workbook workbook,
			String sheetName, int rowNumber, int colNumber) {

		final Sheet sheet = workbook.getSheet(sheetName);
		// read excel File:
		// ================
		return readSheetData(sheet, rowNumber, colNumber);

	}

	// =====================================================================================
	public static List<List<String>> readExcelFile(Workbook workbook,
			int sheetIndex, int rowNumber, int colNumber) {
		try {

			final Sheet sheet = workbook.getSheetAt(sheetIndex);
			// read excel File:
			// ================
			List<List<String>> data = null;
			if (rowNumber >= 0 && colNumber >= 0) {
				data = readSheetData(sheet, rowNumber, colNumber);
			}
			return data;
		} catch (final Exception e) {
			return null;
		}
	}

	// =======================================================================================
	private static List<List<String>> readSheetData(final Sheet sheet,
			int rowNumber, int colNumber) {
		final List<List<String>> allData = new ArrayList<List<String>>();

		// first: get Column count:
		// ========================
		if (rowNumber >= sheet.getLastRowNum() + 1) {
			return null;
		}
		final int columnCount = getColumnCount(sheet.getLastRowNum() + 1,
				sheet, rowNumber);

		if (colNumber >= columnCount) {
			return null;
		}

		for (int i = rowNumber; i < sheet.getLastRowNum() + 1; i++) {
			final List<String> rowData = new ArrayList<String>();
			final Row row = sheet.getRow(i);
			for (int j = colNumber; j < columnCount; j++) {
				Cell cell = null;
				if ((row != null) && (row.getCell((short) j) != null)) {
					cell = row.getCell((short) j);
				}
				String result = getCellContent(cell);
				rowData.add(result);
			}
			allData.add(rowData);
		}

		return allData;
	}

	// =====================================================================================
	public static int getColumnCount(final int rowsCount, final Sheet sheet,
			final int startRow) {
		int colsCount = 0;
		for (int k = startRow; k < rowsCount; k++) {
			if (sheet.getRow(k) != null) {
				if (sheet.getRow(k).getLastCellNum() >= colsCount) {
					colsCount = (sheet.getRow(k).getLastCellNum());
				}
			}
		}
		return colsCount;
	}

	// ======================================================================================
	public static String getCellContent(final Cell cell) {
		String cellContent = "";
		if (cell != null) {
			if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				final Double d1 = Double.valueOf(cell.getNumericCellValue());
				final int i2 = d1.intValue();
				if (i2 == d1) {
					cellContent = i2 + "";
				} else {
					cellContent = cell.getNumericCellValue() + "";
					if (cellContent.contains("E")) {
						final DecimalFormat dc = new DecimalFormat("#.##");
						cellContent = dc.format(cell.getNumericCellValue());
					}
				}
			} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
				cellContent = cell.getBooleanCellValue() + "";
			} else if (cell.getCellType() == Cell.CELL_TYPE_ERROR) {
				cellContent = "";
			} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
				try {
					cellContent = cell.getRichStringCellValue().getString();
				} catch (final Exception e) {
					cell.getCellFormula();
				}
			} else {
				cellContent = cell.getRichStringCellValue().getString();
			}
			cellContent = cellContent.trim();
			cellContent = getEnglishVersion(cellContent);
		}
		return cellContent;
	}

	// ======================================================================================
	private static String getEnglishVersion(final String str) {
		final String cellContent = str;
		StringBuilder englishString = new StringBuilder("");

		for (int k = 0; k < cellContent.length(); k++) {
			final int stringChar = cellContent.charAt(k);
			if (stringChar > 255) {
				continue;
			}
			englishString.append(cellContent.charAt(k));
		}
		return englishString.toString();
	}
	// ======================================================================================
}
