package com.se.excel.reader.utilities;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.reader.template.impl.CellStyleTemplateImpl;

public class ImportUtils {

	public static CellStyleTemplate extractCellStyle(Cell cell) {
		CellStyleTemplate cellStyleTemplate = null;
		if (cell != null) {
			CellStyle cellStyle = cell.getCellStyle();
			if (cellStyle != null) {
				cellStyleTemplate = new CellStyleTemplateImpl();
				cellStyleTemplate.setAlignment(cellStyle.getAlignment());
				cellStyleTemplate.setVerticalAlignment(cellStyle
						.getVerticalAlignment());
				cellStyleTemplate.setBorderBottom(cellStyle.getBorderBottom());
				cellStyleTemplate.setBorderTop(cellStyle.getBorderTop());
				cellStyleTemplate.setBorderLeft(cellStyle.getBorderLeft());
				cellStyleTemplate.setBorderRight(cellStyle.getBorderRight());
				cellStyleTemplate.setFillBackgroundColor(getFillColorAtIndex(
						cellStyle.getFillForegroundColor(), cellStyle, cell
								.getSheet().getWorkbook()));
				extractFont(cellStyle, cellStyleTemplate, cell.getSheet()
						.getWorkbook());
				return cellStyleTemplate;
			}
		}
		return null;

	}

	private static void extractFont(CellStyle cellStyle,
			CellStyleTemplate cellStyleTemplate, Workbook workbook) {

		short number = cellStyle.getFontIndex();
		if (number != -1) {
			org.apache.poi.ss.usermodel.Font fontWorkbook = workbook
					.getFontAt(number);
			String name = fontWorkbook.getFontName();
			int height = fontWorkbook.getFontHeightInPoints();
			int style = -1;
			switch (fontWorkbook.getBoldweight()) {
			case org.apache.poi.ss.usermodel.Font.BOLDWEIGHT_BOLD:
				style = Font.BOLD;
				break;
			case org.apache.poi.ss.usermodel.Font.BOLDWEIGHT_NORMAL:
				style = Font.PLAIN;
				break;
			default:
				style = Font.PLAIN;
			}
			if(fontWorkbook.getUnderline()== org.apache.poi.ss.usermodel.Font.U_SINGLE){
				cellStyleTemplate.setUnderLined(true);
			}
			Font font = new Font(name, style, height);
			cellStyleTemplate.setFont(font);

			cellStyleTemplate.setFillFontColor(getFontColorAtIndex(
					fontWorkbook.getColor(), number, workbook));
		}
	}

	private static Color getFillColorAtIndex(short index, CellStyle cellStyle,
			final Workbook workbook) {
		if (workbook instanceof HSSFWorkbook) {
			HSSFWorkbook hssfWorkbook = (HSSFWorkbook) workbook;
			HSSFColor color = hssfWorkbook.getCustomPalette().getColor(index);
			if (color == null) {
				return Color.WHITE;
			}
			return new Color(color.getTriplet()[0], color.getTriplet()[1],
					color.getTriplet()[2]);
		} else if (workbook instanceof XSSFWorkbook) {
			XSSFCellStyle xssfCellStyle = (XSSFCellStyle) cellStyle;
			byte[] color = xssfCellStyle.getFillBackgroundXSSFColor().getRgb();
			if (color == null) {
				return Color.WHITE;
			}
			return new Color(color[0], color[1], color[2]);
		}
		return null;
	}

	private static Color getFontColorAtIndex(short index, short fontIndex,
			final Workbook workbook) {
		if (workbook instanceof HSSFWorkbook) {
			HSSFWorkbook hssfWorkbook = (HSSFWorkbook) workbook;
			HSSFColor color = hssfWorkbook.getCustomPalette().getColor(index);
			if (color == null) {
				return Color.BLACK;
			}
			return new Color(color.getTriplet()[0], color.getTriplet()[1],
					color.getTriplet()[2]);
		} else if (workbook instanceof XSSFWorkbook) {
			XSSFFont xssfFont = (XSSFFont) workbook.getFontAt(fontIndex);
			byte[] color = xssfFont.getXSSFColor().getRgb();
			if (color == null) {
				return Color.BLACK;
			}
			return new Color(color[0], color[1], color[2]);
		}
		return null;
	}

	public static List<MergedRegion> getMergedRegions(Sheet sheet) {
		List<MergedRegion> mergedRegions = new ArrayList<MergedRegion>();
		int mergedRegionsNumber = sheet.getNumMergedRegions();
		for (int i = 0; i < mergedRegionsNumber; i++) {
			MergedRegion mergedRegion = new MergedRegion();
			mergedRegion.setStartedColumun((sheet.getMergedRegion(i)
					.getFirstColumn()));
			mergedRegion
					.setStartedRow((sheet.getMergedRegion(i).getFirstRow()));
			mergedRegion.setEndedColumun((sheet.getMergedRegion(i)
					.getLastColumn()));
			mergedRegion.setEndedRow((sheet.getMergedRegion(i).getLastRow()));
			mergedRegions.add(mergedRegion);
			Row row = sheet.getRow(mergedRegion.getStartedRow());
			if (row != null) {
				mergedRegion.setRegionStyle(extractCellStyle(row
						.getCell(mergedRegion.getStartedColumun())));
			}
		}

		return mergedRegions;
	}
}
