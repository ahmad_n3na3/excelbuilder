package com.se.excel.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.reader.utilities.ImportUtils;
import com.se.excel.reader.utilities.ReadExcelFile;

public class ExcelReader {
	private Workbook workbook = null;

	public CellStyleTemplate readCellStyle(int rowNumber, int colNumer,
			String sheetName) {
		if (workbook != null) {
			if (sheetName != null && !sheetName.isEmpty()) {
				Sheet sheet = workbook.getSheet(sheetName);
				Row row = sheet.getRow(rowNumber);
				if (row != null) {
					return ImportUtils.extractCellStyle(row.getCell(colNumer));
				}
			}
		}
		return null;
	}

	public List<MergedRegion> extractMergedRegions(String sheetName) {
		if (workbook != null) {
			if (sheetName != null && !sheetName.isEmpty()) {
				Sheet sheet = workbook.getSheet(sheetName);
				return ImportUtils.getMergedRegions(sheet);
			}
		}
		return null;
	}

	public ExcelReader(InputStream inputStream, boolean is2007Format)
			throws IOException {
		if (inputStream != null) {
			if (is2007Format) {
				workbook = new XSSFWorkbook(inputStream);
			} else {
				workbook = new HSSFWorkbook(inputStream);
			}
		}
	}

	public List<List<String>> readExcelData(String sheetName, int rowNumber,
			int colNumber) {
		if (workbook != null) {
			return ReadExcelFile.readExcelFile(workbook, sheetName, rowNumber,
					colNumber);
		} else {
			return null;
		}
	}

	public List<List<String>> readExcelData(int sheetIndex, int rowNumber,
			int colNumber) {
		if (workbook != null) {
			return ReadExcelFile.readExcelFile(workbook, sheetIndex, rowNumber,
					colNumber);
		} else {
			return null;
		}
	}

}
