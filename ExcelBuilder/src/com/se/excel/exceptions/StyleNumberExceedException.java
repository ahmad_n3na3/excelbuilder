package com.se.excel.exceptions;

public class StyleNumberExceedException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = -7449939405965448160L;

	public StyleNumberExceedException(String messag) {
		super(messag);
	}
}
