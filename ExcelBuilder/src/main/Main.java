package main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.se.excel.builder.ExcelBuilder;
import com.se.excel.builder.template.CellStyleTemplate;
import com.se.excel.builder.template.MergedRegion;
import com.se.excel.builder.template.SheetAttribute;
import com.se.excel.reader.ExcelReader;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			 OutputStream outputStream = new FileOutputStream("file3.xls");
			 ExcelBuilder builder = new ExcelBuilder();
			 String[] header = { "header1", "header2", "header3", "header4",
			 "header5", "header6", "header7" };
			 List<List<String>> datatList = new ArrayList<List<String>>();
			 String[] data = null;
			
			 for (int i = 0; i < 1000; i++) {
			 data = new String[] { "data1" + i, "data2" + i, "data3" + i,
			 "data4" + i, "data5" + i, "data6" + i, "data7" + i };
			 datatList.add(Arrays.asList(data));
			 }
			 			
			 builder.createExcelsheet(
			 false,
			 Arrays.asList(header),
			 datatList,
			 "sheet1",
			 "report1",
			 "http://app.siliconexpert.com/CMOMFX/assets/images/siliconexpert_logo.png",
			 null, null, outputStream, new SheetAttribute("User Name",
			 "ahmad neanaa"), new SheetAttribute("Date",
			 new Date().toString()));

//			InputStream inputStream = new FileInputStream(
//					"C:\\HtmlParsed.xls");
//
//			ExcelReader excelReader = new ExcelReader(inputStream, false);
////			CellStyleTemplate cellStyleTemplate = excelReader.readCellStyle(9, 2, "Ads statistics Advertiser Repor");
//			List< MergedRegion> mergedRegions = excelReader.extractMergedRegions("Sheet0");
//			System.out.println(mergedRegions);
//
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
