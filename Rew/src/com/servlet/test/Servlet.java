package com.servlet.test;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Servlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		Writer writer = response.getWriter();

		writer.write("<html><head></head><body>");

		writer.write("<h1>List what in request</h1>");
		Map<String, String[]> map = request.getParameterMap();
		if (map != null && !map.isEmpty()) {
			writer.write("<h3>Parameters</h3>");
			for (Entry<String, String[]> entry : map.entrySet()) {
				writer.write("<div>");
				writer.write("<b>");
				writer.write(entry.getKey());
				writer.write(": </b>");
				for (int i = 0; i < entry.getValue().length; i++) {
					writer.write(entry.getValue()[i] + ",");
				}

				writer.write("</div>");
			}
		}
		Enumeration<String> enumeration = request.getHeaderNames();
		if (enumeration != null && enumeration.hasMoreElements()) {
			writer.write("<h3>Headers</h3>");
			while (enumeration.hasMoreElements()) {
				String string = (String) enumeration.nextElement();
				writer.write("<div>");
				writer.write("<b>");
				writer.write(string);
				writer.write(": </b>");
				writer.write(request.getHeader(string));
				writer.write("</div>");
			}

			Cookie[] cookies = request.getCookies();

			if (cookies != null && cookies.length != 0) {
				writer.write("<h3>Cookies</h3>");
				for (int i = 0; i < cookies.length; i++) {
					writer.write("<div>");
					writer.write("<b>");
					writer.write(cookies[i].getName());
					writer.write(": </b>");
					writer.write(cookies[i].getValue()+",");
					writer.write(cookies[i].getDomain()+",");
					writer.write(cookies[i].getPath()+",");
					writer.write(cookies[i].getMaxAge());
					
					writer.write("</div>");
				}
			}
			
			writer.write("<br/>");
			
			String sessionID = request.getSession().getId();
			writer.write("<div>");
			writer.write("<b>");
			writer.write("Session ID");
			writer.write(": </b>");
			writer.write(sessionID);
			writer.write("</div>");

			writer.write("</body></html>");
		}

	}
}
